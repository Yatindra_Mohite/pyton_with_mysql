# Generated by Django 2.0.2 on 2018-02-03 08:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='catalog',
            name='catalog_type',
            field=models.IntegerField(verbose_name=1),
        ),
    ]
