from django.db import models

# Create your models here.
class catalog(models.Model):
	catalog_name = models.CharField(max_length=30)
	catalog_type = models.IntegerField(1)
	catalog_image = models.CharField(max_length=100)
	crate_date = models.DateTimeField()

	def __str__(self):
		return self.catalog_name

class cloth(models.Model):
	catalog_id = models.ForeignKey(catalog, on_delete=models.CASCADE)
	cloth_name = models.CharField(max_length=80)
	cloth_image = models.CharField(max_length=100)
	crate_date = models.DateTimeField()	

